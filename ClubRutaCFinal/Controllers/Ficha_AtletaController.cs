﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClubRutaCFinal.Models;

namespace ClubRutaCFinal.Controllers
{
    public class Ficha_AtletaController : Controller
    {
        private ModeloClubContainer db = new ModeloClubContainer();

        // GET: Ficha_Atleta
        public ActionResult Index()
        {
            return View(db.Ficha_AtletaSet.ToList());
        }

        public ActionResult Atletas_Detalles()
        {
            return View(db.Ficha_AtletaSet.ToList());
        }

        // GET: Ficha_Atleta/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ficha_Atleta ficha_Atleta = db.Ficha_AtletaSet.Find(id);
            if (ficha_Atleta == null)
            {
                return HttpNotFound();
            }
            return View(ficha_Atleta);
        }

        // GET: Ficha_Atleta/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Ficha_Atleta/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre,Apellido,Documento_Identidad,Celular,Telefono,Fecha_Nacimineto,Edad,RH,Categoria,Direccion,Ciudad,Eps")] Ficha_Atleta ficha_Atleta)
        {
            if (ModelState.IsValid)
            {
                db.Ficha_AtletaSet.Add(ficha_Atleta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(ficha_Atleta);
        }

        // GET: Ficha_Atleta/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ficha_Atleta ficha_Atleta = db.Ficha_AtletaSet.Find(id);
            if (ficha_Atleta == null)
            {
                return HttpNotFound();
            }
            return View(ficha_Atleta);
        }

        // POST: Ficha_Atleta/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre,Apellido,Documento_Identidad,Celular,Telefono,Fecha_Nacimineto,Edad,RH,Categoria,Direccion,Ciudad,Eps")] Ficha_Atleta ficha_Atleta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ficha_Atleta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ficha_Atleta);
        }

        // GET: Ficha_Atleta/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ficha_Atleta ficha_Atleta = db.Ficha_AtletaSet.Find(id);
            if (ficha_Atleta == null)
            {
                return HttpNotFound();
            }
            return View(ficha_Atleta);
        }

        // POST: Ficha_Atleta/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ficha_Atleta ficha_Atleta = db.Ficha_AtletaSet.Find(id);
            db.Ficha_AtletaSet.Remove(ficha_Atleta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
