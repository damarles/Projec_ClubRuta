﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClubRutaCFinal.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Podras Obtener Mayor Informacion en:";

            return View();
        }

        public ActionResult Multimedia()
        {
            ViewBag.Message = "Multimedia";

            return View();
        }

        public ActionResult Noticias()
        {
            ViewBag.Message = "Noticias";

            return View();
        }
    }
}