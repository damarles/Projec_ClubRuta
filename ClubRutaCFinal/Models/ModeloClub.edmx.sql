
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 08/04/2015 00:10:36
-- Generated from EDMX file: D:\USUARIO\Desktop\ClubRutaCFinal\ClubRutaCFinal\Models\ModeloClub.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [ClubRutaC];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Ficha_AtletaSet'
CREATE TABLE [dbo].[Ficha_AtletaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Apellido] nvarchar(max)  NOT NULL,
    [Documento_Identidad] bigint  NOT NULL,
    [Celular] bigint  NOT NULL,
    [Telefono] int  NOT NULL,
    [Fecha_Nacimineto] datetime  NOT NULL,
    [Edad] nvarchar(max)  NOT NULL,
    [RH] nvarchar(max)  NOT NULL,
    [Categoria] nvarchar(max)  NOT NULL,
    [Direccion] nvarchar(max)  NOT NULL,
    [Ciudad] nvarchar(max)  NOT NULL,
    [Eps] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'HistoriaSet'
CREATE TABLE [dbo].[HistoriaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Apellido] nvarchar(max)  NOT NULL,
    [Fecha_nacimiento] datetime  NOT NULL,
    [Prueba] nvarchar(max)  NOT NULL,
    [Marca] nvarchar(max)  NOT NULL,
    [Detalle] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'CalendarioSet'
CREATE TABLE [dbo].[CalendarioSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Fecha] datetime  NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Distancia] nvarchar(max)  NOT NULL,
    [Lugar] nvarchar(max)  NOT NULL,
    [Estado] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'MarcasSet'
CREATE TABLE [dbo].[MarcasSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Apellido] nvarchar(max)  NOT NULL,
    [Fecha_Nacimiento] datetime  NOT NULL,
    [Categoria] nvarchar(max)  NOT NULL,
    [Prueba] nvarchar(max)  NOT NULL,
    [Marca] nvarchar(max)  NOT NULL,
    [Detalle] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'SociosSet'
CREATE TABLE [dbo].[SociosSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Apellido] nvarchar(max)  NOT NULL,
    [Documento] bigint  NOT NULL,
    [Fecha_Nacimiento] datetime  NOT NULL,
    [Ocupacion] nvarchar(max)  NOT NULL,
    [Profesion] nvarchar(max)  NOT NULL,
    [Telefono] bigint  NOT NULL,
    [Direccion] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'InscripcionSet'
CREATE TABLE [dbo].[InscripcionSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Apellido] nvarchar(max)  NOT NULL,
    [Documento] bigint  NOT NULL,
    [Talla_buso] nvarchar(max)  NOT NULL,
    [Edad] int  NOT NULL,
    [Telefono] nvarchar(max)  NOT NULL,
    [Direccion] nvarchar(max)  NOT NULL,
    [Eps] nvarchar(max)  NOT NULL,
    [Fecha_Nacimiento] datetime  NOT NULL
);
GO

-- Creating table 'PaginaSet'
CREATE TABLE [dbo].[PaginaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Mision] nvarchar(max)  NOT NULL,
    [Vision] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Ficha_AtletaSet'
ALTER TABLE [dbo].[Ficha_AtletaSet]
ADD CONSTRAINT [PK_Ficha_AtletaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'HistoriaSet'
ALTER TABLE [dbo].[HistoriaSet]
ADD CONSTRAINT [PK_HistoriaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CalendarioSet'
ALTER TABLE [dbo].[CalendarioSet]
ADD CONSTRAINT [PK_CalendarioSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MarcasSet'
ALTER TABLE [dbo].[MarcasSet]
ADD CONSTRAINT [PK_MarcasSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SociosSet'
ALTER TABLE [dbo].[SociosSet]
ADD CONSTRAINT [PK_SociosSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'InscripcionSet'
ALTER TABLE [dbo].[InscripcionSet]
ADD CONSTRAINT [PK_InscripcionSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PaginaSet'
ALTER TABLE [dbo].[PaginaSet]
ADD CONSTRAINT [PK_PaginaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------