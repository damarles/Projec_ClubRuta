//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ClubRutaCFinal.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Ficha_Atleta
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public long Documento_Identidad { get; set; }
        public long Celular { get; set; }
        public int Telefono { get; set; }
        public System.DateTime Fecha_Nacimineto { get; set; }
        public string Edad { get; set; }
        public string RH { get; set; }
        public string Categoria { get; set; }
        public string Direccion { get; set; }
        public string Ciudad { get; set; }
        public string Eps { get; set; }
    }
}
