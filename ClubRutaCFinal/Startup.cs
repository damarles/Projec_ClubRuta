﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ClubRutaCFinal.Startup))]
namespace ClubRutaCFinal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
